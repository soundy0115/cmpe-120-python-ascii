
def upperCase(inputLetter):
    a = ord(inputLetter)
    if 65 <= a and a <= 90:
        return inputLetter
    elif 97 <= a and a <= 122:
        return chr(a - 32)
    else:
        print("that is not a letter!")
        return False

def lowerCase(inputLetter):
    a = ord(inputLetter)
    if 65 <= a and a <= 90:
        return chr(a + 32)
    elif 97 <= a and a <= 122:
        return inputLetter
    else:
        print("that is not a letter!")
        return

def isAlphabet(inputVar):
    asciiNum = ord(inputVar)
    if (65 <= asciiNum and asciiNum <= 90) or (97 <= asciiNum and asciiNum <=122):
        return True
    else:
        return False

def isDigit(inputVar):
    i = ord(str(inputVar))
    if 48 <= i and i <= 57:
        return True
    else:
        return False

def isSpecialChar(inputVar):
    i = ord(inputVar)
    if (33 <= i and i <= 47) or (58 <= i and i <= 64):
        return True
    else:
        return False




print(upperCase("a"))
print(upperCase("A"))
print(lowerCase("b"))
print(lowerCase("B"))
print(isAlphabet("A"))
print(isAlphabet("!"))
print(isDigit(4))
print(isDigit("!"))
print(isSpecialChar("&"))
print(isSpecialChar("A"))